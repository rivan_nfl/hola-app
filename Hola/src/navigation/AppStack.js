import React, { useEffect } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import SplashScreen from 'react-native-splash-screen'

import MainNavigator from './MainNavigator';
import AppointmentDetail from '../screens/AppointmentDetail';
import SearchTreatment from '../screens/SearchTreatment';
import TreatmentDetail from '../screens/TreatmentDetail';
import BookTreatment from '../screens/BookTreatment';
import ConfirmAddress from '../screens/ConfirmAddress';
import Success from '../screens/Success';

const Stack = createStackNavigator();


const AppStack = () => {
  
  useEffect(() => {
    setTimeout(() => {
      SplashScreen.hide();
    }, 1500)
  }, [])

  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="Main" component={MainNavigator} />
      <Stack.Screen name="Appointment Detail" component={AppointmentDetail} />
      <Stack.Screen name="Search" component={SearchTreatment} />
      <Stack.Screen name="Treatment Detail" component={TreatmentDetail} />
      <Stack.Screen name="Book Treatment" component={BookTreatment} />
      <Stack.Screen name="Confirm Address" component={ConfirmAddress} />
      <Stack.Screen name="Success" component={Success} />
    </Stack.Navigator>
  );
}

export default AppStack;