import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/Ionicons'

import { widthPercentage as wp, heightPercentage as hp } from '../utils/dimension';
import { colors } from '../utils/colors';

import Homepage from '../screens/Homepage';
import Booking from '../screens/Booking';
import Shop from '../screens/Shop';
import Reward from '../screens/Reward';
import Profile from '../screens/Profile';

const Tab = createBottomTabNavigator();

const MainNavigator = () => {
  return (
    <Tab.Navigator screenOptions={({ route }) => ({
      headerShown: false,
      tabBarActiveTintColor: colors.gold,
      tabBarStyle: {
        height: hp * 0.065,
        borderTopWidth: 1,
        borderTopColor: 'rgba(55, 55, 55, 0.1)'
      },
      tabBarLabelStyle: {
        fontSize: wp * 0.03,
        fontWeight: 'bold'
      },
      tabBarIcon: ({ focused, size, color }) => {
        let iconName;

        if(route.name == 'Home') {
          iconName = focused ? 'home' : 'home-outline'
        } else if(route.name == 'Booking') {
          iconName = focused ? 'bookmark' : 'bookmark-outline'
        } else if(route.name == 'Shop') {
          iconName = focused ? 'cart' : 'cart-outline'
        } else if(route.name == 'Reward') {
          iconName = focused ? 'gift' : 'gift-outline'
        } else if(route.name == 'Profile') {
          iconName = focused ? 'person' : 'person-outline'
        }

        return <Icon name={iconName} size={wp * 0.07} color={color} />
      }
    })}>
      <Tab.Screen name="Home" component={Homepage} />
      <Tab.Screen name="Booking" component={Booking} />
      <Tab.Screen name="Shop" component={Shop} />
      <Tab.Screen name="Reward" component={Reward} />
      <Tab.Screen name="Profile" component={Profile} />
    </Tab.Navigator>
  );
}

export default MainNavigator;