import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const Profile = () => {
    return (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <Text style={{fontSize: 30, fontWeight: 'bold', color: 'black'}}>Profile Screen</Text>
            <Text style={{fontSize: 30, fontWeight: 'bold', color: 'gold'}}>Coming Soon</Text>
        </View>
    )
}

export default Profile

const styles = StyleSheet.create({})
