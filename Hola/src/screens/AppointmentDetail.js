import React from 'react'
import { Image, Pressable, ScrollView, StyleSheet, Text, View } from 'react-native'

// Components
import HeaderButton from '../components/HeaderButton'
import Icon from 'react-native-vector-icons/Ionicons'

import { colors } from '../utils/colors'
import { widthPercentage as wp, heightPercentage as hp } from '../utils/dimension'
import therapist from '../assets/therapist.png'
import location from '../assets/location.png'

const AppointmentDetail = ({ navigation }) => {
    return (
        <View style={styles.container}>
            <HeaderButton title='Appointment Detail' onPress={() => navigation.goBack()} />
            <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.content}>
                <View style={styles.confirm}>
                    <Icon name='checkmark-done' size={wp * 0.07} color={'black'} />
                    <View style={{marginLeft: 10}}>
                        <Text style={styles.txt}>Booking Confirmed. Today, 13.00</Text>
                        <Text style={styles.normalTxt}>It's done. Can't wait to see you</Text>
                    </View>
                </View>

                <Text style={[styles.txt, {marginBottom: 10}]}>Upcoming Booking</Text>

                <View style={styles.infoWrapper}>
                    <Icon name='calendar-outline' size={wp * 0.06} color={colors.gold} />
                    <View style={styles.infoTxt}>
                        <Text style={styles.txt}>Date & Time</Text>
                        <Text style={styles.normalTxt}>Rabu, 21 Jan 2021, 14:30</Text>
                    </View>
                </View>
                
                <View style={styles.infoWrapper}>
                    <Icon name='location-outline' size={wp * 0.06} color={colors.gold} />
                    <View style={styles.infoTxt}>
                        <Text style={styles.txt}>Beauty By Appointment, Gunawarman</Text>
                        <Text style={styles.normalTxt}>Jl. Gunawarman No. 57, Kebayoran Baru, Jakarta Selatan</Text>
                        {/* Map Side */}
                        <Image
                            source={location}
                            style={{
                                height: hp * 0.14,
                                width: '100%',
                                marginTop: 5
                            }}
                        />
                    </View>
                </View>

                <View style={styles.infoWrapper}>
                    <Icon name='sunny-outline' size={wp * 0.06} color={colors.gold} />
                    <View style={styles.infoTxt}>
                        <Text style={styles.txt}>Services</Text>
                        <Text style={styles.normalTxt}>1 x Eyelash Variant 2</Text>
                        <Text style={styles.goldTxt}>Rp. 250.000</Text>
                    </View>
                </View>

                <Text style={[styles.txt, {marginVertical: 12}]}>Therapist Name</Text>

                <View style={styles.therapist}>
                    <Image source={therapist} style={styles.therapistImg} />
                    <Text style={styles.therapistName}>Tania Belisa</Text>
                </View>

                <View style={styles.card}>
                    <Icon name='heart' size={wp * 0.06} color={colors.gold} />
                    <Text style={[styles.txt, {marginLeft: 10}]}>Health Card</Text>
                </View>
                <View style={[styles.card, {borderTopWidth: 0}]}>
                    <Icon name='checkmark-circle-outline' size={wp * 0.06} color={colors.gold} />
                    <Text style={[styles.normalTxt, {marginLeft: 10}]}>
                        Last Antigen Tested on
                        <Text style={{color: 'black', fontWeight: 'bold'}}> August 12</Text>
                    </Text>
                </View>

            </View>
            </ScrollView>

            <View style={styles.footer}>
                <Pressable style={styles.btn} onPress={() => alert('Reschedule Button')} android_disableSound>
                    <Text style={styles.btnTxt}>Reschedule</Text>
                    <Icon name='logo-whatsapp' size={wp * 0.06} color={'black'} />
                </Pressable>
                <Pressable style={[styles.btn, styles.btnDirection]} onPress={() => alert('Get Directions Button')} android_disableSound>
                    <Icon name='paper-plane-outline' size={wp * 0.06} color={'black'} />
                    <Text style={styles.btnTxt}>Get Directions</Text>
                </Pressable>
            </View>
        </View>
    )
}

export default AppointmentDetail

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white
    },
    content: {
        flex: 1,
        padding: 16
    },
    confirm: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 10
    },
    txt: {
        fontWeight: 'bold',
        fontSize: wp * 0.04,
        color: 'black'
    },
    normalTxt: {
        fontSize: wp * 0.035,
        color: 'black'
    },
    goldTxt: {
        color: colors.gold, 
        fontWeight: 'bold', 
        fontSize: wp * 0.035, 
        marginTop: 5
    },
    infoWrapper: {
        flexDirection: 'row',
        marginBottom: 10
    },
    infoTxt: {
        marginLeft: 10,
        flex: 1
    },
    therapist: {
        flexDirection: 'row',
        marginBottom: 12
    },
    therapistImg: {
        height: wp * 0.17,
        borderRadius: (wp * 0.17) / 2,
        marginRight: 12
    },
    therapistName: {
        fontSize: wp * 0.05, 
        color: 'black', 
        fontWeight: 'bold'
    },
    card: {
        flexDirection: 'row',
        paddingHorizontal: 10,
        paddingVertical: 7,
        borderWidth: 1,
        borderColor: 'rgba(55, 55, 55, 0.1)',
        borderRadius: 4
    },
    footer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        paddingHorizontal: 16,
        paddingVertical: 10,
    },
    btn: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        borderWidth: 1,
        borderRadius: 8,
        width: '45%',
        paddingVertical: 8,
        paddingHorizontal: 10,
    },
    btnDirection: {
        backgroundColor: '#CA965C',
        borderWidth: 0,
    },
    btnTxt: {
        fontWeight: 'bold',
        fontSize: wp * 0.04,
        color: 'black'
    }
})
