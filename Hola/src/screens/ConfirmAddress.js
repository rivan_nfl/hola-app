import React, { useState } from 'react'
import { Image, Pressable, ScrollView, StyleSheet, Text, TextInput, View } from 'react-native'

import HeaderButton from '../components/HeaderButton'
import map from '../assets/map.png'

import { widthPercentage as wp, heightPercentage as hp } from '../utils/dimension'
import Icon from 'react-native-vector-icons/Ionicons'
import { colors } from '../utils/colors'

const ConfirmAddress = ({ navigation }) => {
    const [location, setLocation] = useState()
    const [address, setAddress] = useState()

    return (
        <View style={styles.container}>
            <HeaderButton title='Confirm Address' onPress={() => navigation.goBack()} />
            <ScrollView showsVerticalScrollIndicator={false}>
            <Image source={map} style={styles.img} />
            <View style={styles.content}>
                <Text style={styles.title}>Address Detail</Text>
                <View style={{flexDirection: 'row', alignItems: 'center', marginBottom: 16}}>
                    <Icon name='location' color={colors.gold} size={wp * 0.06} style={{marginRight: 12}} />
                    <Text style={styles.normalTxt} numberOfLines={2}>Jl Kedung Halang I No. 46 RT 002/RW 012, Bekasi Selatan</Text>
                </View>

                <View style={styles.inputContainer}>
                    <Icon name='home-outline' color={colors.gold} size={wp * 0.06} style={{marginRight: 12}} />
                    <TextInput 
                        placeholder='Location Name' 
                        placeholderTextColor={'gray'} 
                        style={styles.txtInput}
                        onChangeText={text => setLocation(text)}
                        value={location}
                        />
                </View>
                <View style={styles.inputContainer}>
                    <Icon name='pencil-outline' color={colors.gold} size={wp * 0.06} style={{marginRight: 12}} />
                    <TextInput 
                        placeholder='Address Details : Jl Tebet Raya...' 
                        placeholderTextColor={'gray'} 
                        style={styles.txtInput}
                        multiline
                        onChangeText={text => setAddress(text)}
                        value={address}
                    />
                </View>
                <View style={styles.inputContainer}>
                    <Icon name='chatbubble-ellipses-outline' color={colors.gold} size={wp * 0.06} style={{marginRight: 12}} />
                    <TextInput 
                        placeholder='Address Notes' 
                        placeholderTextColor={'gray'} 
                        style={styles.txtInput}
                        multiline
                    />
                </View>
            </View>
            </ScrollView>
            <View style={styles.footer}>
                <Pressable style={styles.btn} onPress={() => {
                    !location || !address
                    ? alert('Please fill in your Address')
                    : navigation.navigate('Book Treatment', {location, address})
                }}>
                    <Text style={styles.btnTxt}>Set Location</Text>
                </Pressable>
            </View>
        </View>
    )
}

export default ConfirmAddress

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    content: {
        flex: 1,
        padding: 16
    },
    img: {
        width: '100%',
        height: hp * 0.2
    },
    title: {
        fontSize: wp * 0.04,
        fontWeight: 'bold',
        color: 'black',
        marginBottom: 16
    },
    normalTxt: {
        color: 'black',
        fontSize: wp * 0.035,
        flex: 1
    },
    inputContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 5,
        paddingHorizontal: 8,
        borderWidth: 1,
        borderColor: 'rgba(55, 55, 55, 0.2)',
        borderRadius: 8,
        marginBottom: 14
    },
    txtInput: {
        color: 'black',
        fontSize: wp * 0.04,
        flex: 1,
        flexWrap: 'wrap'
    },
    footer: {
        paddingHorizontal: 16,
        paddingVertical: 10,
    },
    btn: {
        backgroundColor: colors.gold,
        paddingVertical: 8,
        alignItems: 'center',
        borderRadius: 10
    },
    btnTxt: {
        fontSize: wp * 0.045,
        fontWeight: 'bold',
        color: 'black'
    }
})
