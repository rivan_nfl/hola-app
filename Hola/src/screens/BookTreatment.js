import React, { useEffect, useState } from 'react'
import { Pressable, ScrollView, StyleSheet, Text, TextInput, View } from 'react-native'

import Icon from 'react-native-vector-icons/Ionicons'
import Font from 'react-native-vector-icons/FontAwesome5'
import DateTimePicker from '@react-native-community/datetimepicker';

import HeaderButton from '../components/HeaderButton'
import ServiceModal from '../components/ServiceModal'

import { widthPercentage as wp } from '../utils/dimension'
import { colors } from '../utils/colors'

const BookTreatment = ({ navigation, route }) => {
    const [activeTime, setActiveTime] = useState(null);
    const [modalVisible, setModalVisible] = useState(false);
    const [address, setAddress] = useState(route.params);
    const [date, setDate] = useState(new Date());
    const [datePicked, setDatePicked] = useState(false);
    const [mode, setMode] = useState('date');
    const [show, setShow] = useState(false);
    const [serviceType, setServiceType] = useState({
        loc: 'BBA Gunawarman',
        address: 'Jl Kemang Raya 18D, Jakarta Selatan',
        icon: 'business'
    })

    const time = [
        '12.30', '13.00', '13.30', '14.00', '14.30', '15.00', '15.30', '16.00', '16.30', '17.00'
    ];

    const handleCloseModal = (item) => {
        if(item == null) return setModalVisible(!modalVisible)

        setServiceType(item)
        return setModalVisible(!modalVisible)
    }

    const onChangeDate = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow(Platform.OS === 'ios');
        setDate(currentDate);
        setDatePicked(true)
    };

    const showDatepicker = () => {
        setShow(true);
        setMode('date');
    };

    const changeMonth = (month) => {
        return month == 0
        ? 'January'
        : month == 1
        ? 'February'
        : month == 2
        ? 'March'
        : month == 3
        ? 'April'
        : month == 4
        ? 'May'
        : month == 5
        ? 'June'
        : month == 6
        ? 'July'
        : month == 7
        ? 'August'
        : month == 8
        ? 'September'
        : month == 9
        ? 'October'
        : month == 10
        ? 'November'
        : 'December'
    }

    useEffect(() => setAddress(route.params), [route.params])

    return (
        <View style={styles.container}>
            <HeaderButton title='Book Treatment' onPress={() => navigation.goBack()} />
            <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.content}>
                <View style={styles.contentContainer}>
                    <Text style={styles.title}>Services Type</Text>
                    <Pressable style={styles.servicesLoc} onPress={() => setModalVisible(!modalVisible)} android_disableSound>
                        <View style={styles.locIcon}>
                            <Icon name={serviceType.icon} size={wp * 0.07} color={colors.gold} />
                        </View>
                        <View style={{flex: 1}}>
                            <Text style={styles.medTxt}>{serviceType.loc}</Text>
                            <Text>{serviceType.address}</Text>
                        </View>
                            <Icon name='chevron-forward-circle' size={wp * 0.07} color={colors.gold} />
                    </Pressable>
                </View>

                <View style={[styles.contentContainer]}>
                    <Text style={styles.title}>Select Date</Text>
                    <Pressable
                        style={styles.calendar}
                        onPress={showDatepicker}
                    >
                        <Icon name='calendar-outline' size={wp * 0.06} color={colors.gold} />
                        {
                            datePicked
                            ? <Text style={styles.dateTxt}>{date.getDate()} / {changeMonth(date.getMonth())} / {date.getFullYear()}</Text>
                            : <Text style={styles.dateTxt}>DD / MM / YY</Text>
                        }
                    </Pressable>

                    {show && (
                        <DateTimePicker
                            testID="dateTimePicker"
                            value={date}
                            mode={mode}
                            is24Hour={true}
                            display="default"
                            onChange={onChangeDate}
                            minimumDate={new Date()}
                        />
                    )}
                    
                    <Text style={styles.title}>Time</Text>

                    {
                        serviceType.loc == 'Lash Studio'
                        ? <View style={styles.fullyBooked}>
                            <Text style={styles.fullyBookedTxt}>Sorry, we're fully booked today, please select another day</Text>
                        </View>
                        : <>
                            <View style={styles.chooseTimeWrapper}>
                                {
                                    time.map((item, index) => (
                                        <Pressable
                                            key={index} 
                                            style={activeTime == index ? styles.choosenTime : styles.chooseTime}
                                            onPress={() => setActiveTime(index)}
                                        >
                                            {
                                                activeTime == index &&
                                                <Icon name='checkmark-circle' size={wp * 0.045} color={'black'} style={{marginRight: 5}} />
                                            }
                                            <Text style={styles.chooseTimeTxt}>{item}</Text>
                                        </Pressable>
                                    ))
                                }
                            </View>
                        </>
                    }
                </View>

                {
                    serviceType.loc == 'Home Service'
                    ? <>
                        {
                            address
                            ? <Pressable
                                style={[styles.contentContainer, styles.contentAdded]}
                                onPress={() => navigation.navigate('Confirm Address')}
                            >
                                <View>
                                    <Text style={[styles.title]}>{route.params.location}</Text>
                                    <Text style={{color: 'black'}}>{route.params.address}</Text>
                                </View>
                                <Icon name='chevron-forward-outline' size={wp * 0.06} color={colors.gold} style={{marginRight: 5}} />
                            </Pressable>
                            : <Pressable
                                style={[styles.contentContainer, styles.contentAdded]}
                                onPress={() => navigation.navigate('Confirm Address')}
                            >
                                <Text style={[styles.title, {marginBottom: 0}]}>Input Your Address</Text>
                                <Icon name='chevron-forward-outline' size={wp * 0.06} color={colors.gold} style={{marginRight: 5}} />
                            </Pressable>
                        }
                    </> 
                    : null
                }

                {/* Address */}
                

                <View style={[styles.contentContainer, {flex: 1, marginBottom: 0}]}>
                    <Text style={styles.title}>Services</Text>
                    <Text style={[styles.title, {marginBottom: 5}]}>1x Treatment Variant 2</Text>
                    <Text style={{fontWeight: 'bold', color: colors.gold}}>Rp 250.000</Text>
                    
                    <View style={styles.promo}>
                        <Font name='percent' size={wp * 0.05} color={colors.gold} />
                        <TextInput placeholder='Input Promo Code' style={[styles.medTxt, {flex: 1, marginLeft: 10, paddingVertical: 5}]} />
                        <View style={styles.btn}>
                            <Text style={styles.medTxt}>Apply</Text>
                        </View>
                    </View>
                </View>
            </View>
            </ScrollView>

            <ServiceModal
                modalVisible={modalVisible}
                closeModal={handleCloseModal}
            />

            <View style={styles.footer}>
                <View style={styles.footerTxt}>
                    <Text style={styles.priceTxt}>Total Treatment</Text>
                    <Text style={styles.priceTxt}>Rp 250.000</Text>
                </View>
                <View style={styles.footerTxt}>
                    <Text style={styles.priceTxt}>Admin Fee</Text>
                    <Text style={styles.priceTxt}>Rp 15.000</Text>
                </View>
                <View style={styles.footerTxt}>
                    <Text style={[styles.priceTxt, {fontWeight: 'bold'}]}>Total Down Payment</Text>
                    <Text style={[styles.priceTxt, {fontWeight: 'bold'}]}>Rp 265.000</Text>
                </View>
                <Pressable
                    onPress={() =>{
                        activeTime != null &&
                        datePicked &&
                        serviceType.loc != 'Lash Studio'
                        ? navigation.navigate('Success')
                        : null
                    }}
                    style={
                        activeTime != null &&
                        datePicked &&
                        serviceType.loc != 'Lash Studio'
                        ? styles.footerBtn
                        : styles.footerBtnDisable
                    }
                >
                    <Text
                        style={
                            activeTime != null &&
                            datePicked &&
                            serviceType.loc != 'Lash Studio'
                            ? styles.footerBtnTxt
                            : styles.footerBtnTxtDisable
                            }
                    >Select Payment</Text>
                </Pressable>
            </View>
        </View>
    )
}

export default BookTreatment

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    content: {
        flex: 1,
        backgroundColor: 'rgba(55, 55, 55, 0.05)'
    },
    contentContainer: {
        backgroundColor: 'white', 
        marginBottom: 8, 
        padding: 16
    },
    contentAdded: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    title: {
        fontSize: wp * 0.04,
        fontWeight: 'bold',
        color: 'black',
        marginBottom: 12
    },
    servicesLoc: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    locIcon: {
        padding: 5,
        backgroundColor: 'rgba(55, 55, 55, 0.07)',
        borderRadius: 8,
        marginRight: 12
    },
    medTxt: {
        fontSize: wp * 0.035,
        fontWeight: 'bold',
        color: 'black',
    },
    calendar: {
        flexDirection: 'row',
        alignItems: 'center',
        borderWidth: 1, 
        borderColor: 'rgba(55, 55, 55, 0.2)',
        borderRadius: 8,
        paddingHorizontal: 12,
        paddingVertical: 8,
        marginBottom: 12
    },
    dateTxt: {
        fontSize: wp * 0.04,
        color: 'black',
        marginLeft: 12
    },
    fullyBooked: {
        padding: 16,
        marginHorizontal: 14,
        borderRadius: 8,
        borderWidth: 1,
        borderColor: 'red'
    },
    fullyBookedTxt: {
        color: 'red',
        textAlign: 'center'
    },
    chooseTimeWrapper: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'center'
    },
    chooseTime: {
        flexDirection: 'row',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: 'black',
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderRadius: 100,
        marginHorizontal: 5,
        marginBottom: 10
    },
    choosenTime: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: colors.gold,
        borderWidth: 1,
        borderColor: 'black',
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderRadius: 100,
        marginHorizontal: 4,
        marginBottom: 10
    },
    chooseTimeDisable: {
        borderWidth: 1,
        borderColor: 'gray',
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderRadius: 100,
        marginHorizontal: 5,
        marginBottom: 10
    },
    chooseTimeTxt: {
        color: 'black'
    },
    chooseTimeTxtDisable: {
        color: 'gray'
    },
    promo: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: 7,
        backgroundColor: 'rgba(55, 55, 55, 0.07)',
        borderRadius: 8,
        marginTop: 10,
    },
    btn: {
        backgroundColor: colors.gold,
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderRadius: 8
    },
    footer: {
        paddingHorizontal: 16,
        paddingVertical: 12,
        backgroundColor: 'white',
        borderTopWidth: 1,
        borderColor: 'rgba(55, 55, 55, 0.1)'
    },
    footerTxt: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 10,
    },
    priceTxt: {
        color: 'black',
        fontSize: wp * 0.04
    },
    footerBtn: {
        paddingVertical: 10,
        backgroundColor: colors.gold,
        borderRadius: 8,
        alignItems: 'center'
    },
    footerBtnTxt: {
        color: 'black',
        fontSize: wp * 0.04,
        fontWeight: 'bold'
    },
    footerBtnDisable: {
        paddingVertical: 10,
        backgroundColor: 'rgba(55, 55, 55, 0.3)',
        borderRadius: 8,
        alignItems: 'center'
    },
    footerBtnTxtDisable: {
        color: 'white',
        fontSize: wp * 0.04,
        fontWeight: 'bold'
    }
})
