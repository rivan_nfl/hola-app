import React from 'react'
import { Pressable, StyleSheet, Text, View } from 'react-native'
import { colors } from '../utils/colors'

const Success = ({ navigation }) => {
    return (
        <View style={styles.container}>
            <Text style={styles.title}>Thank you for your Trust to our Service</Text>
            <Text style={styles.desc}>Your Booking Treatment is Succcessful</Text>
            <Pressable style={styles.btn} onPress={() => navigation.navigate('Home', {appointment: true})}>
                <Text style={styles.btnTxt}>Click here to go back to Home Screen</Text>
            </Pressable>
        </View>
    )
}

export default Success

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    title: {
        fontSize: 25,
        color: colors.gold,
        fontWeight: 'bold',
        textAlign: 'center',
    },
    desc: {
        fontSize: 15,
        color: 'black',
        marginVertical: 12
    },
    btn: {
        paddingVertical: 10,
        paddingHorizontal: 14,
        borderRadius: 10,
        alignItems: 'center',
        backgroundColor: colors.gold
    },
    btnTxt: {
        fontSize: 15,
        color: 'black',
        fontWeight: 'bold',
    }
})
