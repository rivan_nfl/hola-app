import React, { useEffect, useState } from 'react'
import { Image, Pressable, ScrollView, StyleSheet, Text, View } from 'react-native'

// Components
import Header from '../components/Header'
import Icon from 'react-native-vector-icons/Ionicons'
import CarouselHome from '../components/CarouselHome'
import upcoming from'../assets/upcoming.png'

// Utils
import { colors } from '../utils/colors'
import { widthPercentage as wp } from '../utils/dimension'

const Homepage = ({ navigation, route }) => {
    const [carouselWidth, setCarouselWidth] = useState(wp - 32)
    const [upcomingAppointment, setUpcomingAppointment] = useState(wp - 32)
    const services = [
        {image: require('../assets/service-1.png')},
        {image: require('../assets/service-2.png')},
        {image: require('../assets/service-3.jpeg')},
        {image: require('../assets/service-4.jpeg')},
    ]

    useEffect(() => setUpcomingAppointment(route.params?.appointment), [route.params])

    return (
        <View style={styles.container}>
            <Header />
            <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.content}>

                <Text style={styles.txt}>Hi Wilona,</Text>
                <Text style={styles.normalTxt}>How can we help you today ?</Text>

                <View style={styles.info}>
                    <View style={styles.points}>
                        <Icon name='sunny' size={wp * 0.08} color={colors.gold} />
                        <View style={{marginLeft: 10}}>
                            <Text style={[styles.normalTxt, {fontWeight: 'bold'}]}>25</Text>
                            <Text>Total Treatment</Text>
                        </View>
                    </View>
                    <View style={styles.points}>
                        <Icon name='journal' size={wp * 0.08} color={colors.gold} />
                        <View style={{marginLeft: 10}}>
                            <Text style={[styles.normalTxt, {fontWeight: 'bold'}]}>Rp. 150.000</Text>
                            <Text>Hola Points</Text>
                        </View>
                    </View>
                </View>

                <View onLayout={(event) => setCarouselWidth(event.nativeEvent.layout.width)}>
                    <CarouselHome width={carouselWidth} />
                </View>

                {upcomingAppointment && <>
                    <Text style={[styles.txt, {marginVertical: 12}]}>Upcoming Appointment</Text>
                    <Pressable
                        style={styles.upcoming}
                        onPress={() => navigation.navigate('Appointment Detail')}
                        android_disableSound
                    >
                        <Image source={upcoming} style={{height: wp * 0.25}} />
                        <View style={styles.upcomingTxtContainer}>
                            <Text style={[styles.normalTxt, {fontWeight: 'bold'}]}>Facial</Text>
                            <Text style={[styles.normalTxt, {fontSize: wp * 0.035}]}>BBA, Gunawarman</Text>
                            <Text style={styles.smallTxt}>Jun 12 2021, 14:30</Text>
                        </View>
                        <Icon name='arrow-forward-circle' size={wp * 0.08} color={'black'} />
                    </Pressable>
                </>
                }

                <Text style={[styles.txt, {marginTop: 14}]}>Book Our Services</Text>

                <View style={styles.servicesWrapper}>
                    {
                        services.map((item, index) => (
                            <Pressable key={index} onPress={() => navigation.navigate('Search')}>
                                <Image
                                    source={item.image}
                                    style={styles.services}
                                />
                            </Pressable>
                        ))
                    }
                </View>
            </View>
            </ScrollView>
        </View>
    )
}

export default Homepage

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.white
    },
    content: {
        flex: 1,
        padding: 16,
    },
    info: {
        flexDirection: 'row',
        marginVertical: 14
    },
    points: {
        flexDirection: 'row',
        alignItems: 'center',
        marginRight: 15
    },
    txt: {
        fontWeight: 'bold',
        fontSize: wp * 0.05,
        color: 'black'
    },
    normalTxt: {
        fontSize: wp * 0.04,
        color: 'black'
    },
    upcoming: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: colors.pink,
        paddingRight: 16,
        borderRadius: 8,
        overflow: 'hidden',
    }, 
    upcomingTxtContainer: {
        marginLeft: 12,
        flex: 1
    },
    smallTxt: {
        color: colors.darkBlue, 
        fontWeight: 'bold', 
        fontSize: wp * 0.03, 
        marginTop: 5
    },
    servicesWrapper: {
        flex: 1, 
        flexDirection: 'row', 
        flexWrap: 'wrap', 
        justifyContent: 'space-between',
        marginVertical: 14
    },
    services: {
        height: wp * 0.25, 
        width: wp * 0.45, 
        borderRadius: 8, 
        marginBottom: 7
    }
})