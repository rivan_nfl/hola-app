import React, { useState } from 'react'
import { Image, Pressable, StyleSheet, Text, View } from 'react-native'

import Icon from 'react-native-vector-icons/Ionicons'
import { widthPercentage as wp, heightPercentage as hp } from '../utils/dimension'

import treatment from '../assets/treatment.png'
import { colors } from '../utils/colors'
import HealthModal from '../components/HealthModal'

const TreatmentDetail = ({ navigation }) => {
    const [modalHealthVisible, setModalHealthVisible] = useState(false);

    const handleHealthModal = () => {
        navigation.navigate('Book Treatment')
        setModalHealthVisible(!modalHealthVisible)
    }

    return (
        <View style={styles.container}>
            <View>
                <Pressable style={styles.btnCancel} onPress={() => navigation.goBack()} android_disableSound>
                    <Icon name='close-outline' size={wp * 0.065} color={'black'} />
                </Pressable>
                <Image source={treatment} style={{width: '100%', height: hp * 0.3}} />
            </View>
            <View style={styles.content}>
                <Text style={styles.title}>Treatment Variant 1</Text>
                <Text style={styles.subtitle}>Eyelash</Text>
                <Text style={styles.description}>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Laboriosam inventore vitae deleniti asperiores totam suscipit, voluptates, nesciunt blanditiis veritatis autem quia repellat harum doloribus quisquam? Modi doloremque ipsa quod placeat!</Text>
            </View>

            <HealthModal
                modalVisible={modalHealthVisible}
                closeModal={() => setModalHealthVisible(!modalHealthVisible)}
                handleHealthModal={handleHealthModal}
            />
            
            <View style={styles.footer}>
                <View>
                    <Text style={styles.price}>Price</Text>
                    <Text style={styles.priceTag}>Rp 250.000</Text>
                </View>
                <Pressable style={styles.btn} onPress={() => setModalHealthVisible(!modalHealthVisible)} android_disableSound>
                    <Text style={styles.btnTxt}>Choose Treatment</Text>
                </Pressable>
            </View>
        </View>
    )
}

export default TreatmentDetail

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    content: {
        flex: 1,
        padding: 16,
    },
    btnCancel: {
        position: 'absolute',
        backgroundColor: 'white',
        borderRadius: 200,
        zIndex: 1,
        top: 15,
        left: 15,
        padding: 2,
    },
    title: {
        fontWeight: 'bold',
        fontSize: wp * 0.05,
        color: 'black',
        marginBottom: 5
    },
    subtitle: {
        color: 'black',
        fontSize: wp * 0.04,
        marginBottom: 10
    },
    description: {
        lineHeight: 20
    },
    footer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 16,
        paddingVertical: 15,
    },
    price: {
        fontSize: wp * 0.035,
        color: colors.darkBlue,
    },
    priceTag: {
        fontSize: wp * 0.05,
        color: '#CA965C',
        fontWeight: 'bold'
    },
    btn: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-around',
        borderRadius: 8,
        width: '50%',
        paddingVertical: 8,
        paddingHorizontal: 10,
        backgroundColor: '#CA965C',
    },
    btnTxt: {
        fontWeight: 'bold',
        fontSize: wp * 0.04,
        color: 'black'
    }
})
