import React, { useState } from 'react'
import { Pressable, ScrollView, StyleSheet, Text, View } from 'react-native'

import HeaderSearch from '../components/HeaderSearch'
import Icon from 'react-native-vector-icons/Ionicons'
import Font from 'react-native-vector-icons/FontAwesome5'

import { widthPercentage as wp } from '../utils/dimension'
import { colors } from '../utils/colors'
import ServiceModal from '../components/ServiceModal'
import HealthModal from '../components/HealthModal'

const SearchTreatment = ({ navigation }) => {
    const [modalVisible, setModalVisible] = useState(false);
    const [isOpen, setIsOpen] = useState(false);
    const [serviceType, setServiceType] = useState({
        loc: 'On Clinic',
        address: 'Choose Clinic Location',
        icon: 'sunny'
    })

    const handleCloseModal = (item) => {
        if(item == null) return setModalVisible(!modalVisible)

        setServiceType(item)
        return setModalVisible(!modalVisible)
    }

    return (
        <View style={styles.container}>
            <HeaderSearch onPress={() => navigation.goBack()} />
            <View style={styles.content}>
            <ScrollView showsVerticalScrollIndicator={false}>
                <View style={styles.contentContainer}>
                    <Text style={styles.title}>Services Type</Text>
                    <Pressable
                        style={styles.servicesLoc}
                        onPress={() => setModalVisible(!modalVisible)}
                        android_disableSound
                    >
                        <View style={styles.locIcon}>
                            <Icon name={serviceType.icon} size={wp * 0.07} color={colors.gold} />
                        </View>
                        <View style={{flex: 1}}>
                            <Text style={styles.medTxt}>{serviceType.loc}</Text>
                            <Text>{serviceType.address}</Text>
                        </View>
                            <Icon name='chevron-forward-circle' size={wp * 0.07} color={colors.gold} />
                    </Pressable>
                </View>

                <View style={{height: 8, backgroundColor: 'rgba(55, 55, 55, 0.07)'}} />

                <View style={[styles.contentContainer, {flex: 1, marginBottom: 0}]}>
                    <Text style={[styles.title, {fontSize: wp * 0.05}]}>Eyelash</Text>
                    <Text>Product short description</Text>

                    <View style={styles.promo}>
                        <Font name='percent' size={wp * 0.05} color={colors.gold} />
                        <Text style={[styles.medTxt, {flex: 1, marginLeft: 10}]}>EyelashPromo50%</Text>
                        <View style={styles.btn}>
                            <Text style={styles.medTxt}>Copy</Text>
                        </View>
                    </View>

                    <Pressable style={styles.subCategory} onPress={() => setIsOpen(!isOpen)}>
                        <View style={{flex: 1}}>
                            <Text style={[styles.title, {marginBottom: 5}]}>Sub Category</Text>
                            <Text style={{width: '80%'}} numberOfLines={2}>Treatment description Lorem ipsum dolor sit amet consectetur, adipisicing elit.</Text>
                        </View>
                        <Icon name={isOpen ? 'chevron-up' : 'chevron-down'} size={wp * 0.07} color={'black'} />
                    </Pressable>

                    {
                        isOpen && <>
                            <View style={styles.subCategory}>
                                <View style={{flex: 1}}>
                                    <Text style={[styles.title, {marginBottom: 5}]}>Treatment Variant 1</Text>
                                    <Text style={{width: '80%'}} numberOfLines={2}>Treatment description Lorem ipsum dolor sit amet consectetur, adipisicing elit.</Text>
                                    <Text style={styles.medTxt}>tap to learn more</Text>
                                    <Text style={{fontWeight: 'bold', color: colors.gold}}>Rp 250.000</Text>
                                </View>
                                <Pressable style={styles.btn} onPress={() => navigation.navigate('Treatment Detail')} android_disableSound>
                                    <Text style={styles.medTxt}>Book</Text>
                                </Pressable>
                            </View>
                            
                            <View style={styles.subCategory}>
                                <View style={{flex: 1}}>
                                    <Text style={[styles.title, {marginBottom: 5}]}>Treatment Variant 2</Text>
                                    <Text style={{width: '80%'}} numberOfLines={2}>Treatment description Lorem ipsum dolor sit amet consectetur, adipisicing elit.</Text>
                                    <Text style={styles.medTxt}>tap to learn more</Text>
                                    <Text style={{fontWeight: 'bold', color: colors.gold}}>Rp 250.000</Text>
                                </View>
                                <Pressable style={styles.btn} onPress={() => navigation.navigate('Treatment Detail')} android_disableSound>
                                    <Text style={styles.medTxt}>Book</Text>
                                </Pressable>
                            </View>
                        </>
                    }

                    <View style={styles.subCategory}>
                        <View style={{flex: 1}}>
                            <Text style={styles.title}>Sub Category</Text>
                        </View>
                        <Icon name='chevron-down' size={wp * 0.07} color={'black'} />
                    </View>
                </View>
                <ServiceModal
                    modalVisible={modalVisible}
                    closeModal={handleCloseModal}
                />
            </ScrollView>
            </View>
        </View>
    )
}

export default SearchTreatment

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    content: {
        flex: 1,
        backgroundColor: 'white', 
    },
    contentContainer: {
        padding: 16
    },
    title: {
        fontSize: wp * 0.04,
        fontWeight: 'bold',
        color: 'black',
        marginBottom: 12
    },
    servicesLoc: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    locIcon: {
        padding: 5,
        backgroundColor: 'rgba(55, 55, 55, 0.07)',
        borderRadius: 8,
        marginRight: 12
    },
    medTxt: {
        fontSize: wp * 0.035,
        fontWeight: 'bold',
        color: 'black',
    },
    promo: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: 7,
        backgroundColor: 'rgba(55, 55, 55, 0.07)',
        borderRadius: 8,
        marginTop: 10,
        marginBottom: 14
    },
    btn: {
        backgroundColor: colors.gold,
        paddingHorizontal: 10,
        paddingVertical: 5,
        borderRadius: 8
    },
    subCategory: {
        flexDirection: 'row',
        alignItems: 'center',
        paddingBottom: 10,
        borderBottomWidth: 2,
        borderColor: 'rgba(55, 55, 55, 0.07)',
        marginBottom: 10
    }
})
