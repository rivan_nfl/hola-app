import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const Reward = () => {
    return (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
            <Text style={{fontSize: 30, fontWeight: 'bold', color: 'black'}}>Reward Screen</Text>
            <Text style={{fontSize: 30, fontWeight: 'bold', color: 'gold'}}>Coming Soon</Text>
        </View>
    )
}

export default Reward

const styles = StyleSheet.create({})
