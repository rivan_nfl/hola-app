import React, { useRef } from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'
import Carousel from 'react-native-snap-carousel';

import { widthPercentage as wp, heightPercentage as hp } from '../utils/dimension';

import carousel from '../assets/carousel.jpeg'

const CarouselHome = ({ width }) => {
    const cref = useRef()
    const data = [1, 2, 3]

    const renderItem = ({item, index}) => {
        return (
            <View style={{borderRadius: 8, overflow: 'hidden'}}>
                <Image source={carousel} style={{width: '100%', height: hp * 0.18}} />
            </View>
        );
    }

    return (
        <Carousel
            ref={(c) => cref.current = c}
            data={data}
            renderItem={renderItem}
            sliderWidth={width}
            itemWidth={width * 0.87}
            contentContainerCustomStyle={{paddingLeft: 0}}
        />
    )
}

export default CarouselHome

const styles = StyleSheet.create({})
