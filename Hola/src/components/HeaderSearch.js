import React from 'react'
import { Pressable, StyleSheet, Text, TextInput, View } from 'react-native'

import Icon from 'react-native-vector-icons/Ionicons'
import { colors } from '../utils/colors'
import { widthPercentage as wp } from '../utils/dimension'

const HeaderSearch = ({ onPress }) => {
    return (
        <View style={styles.container}>
            <Pressable onPress={onPress} android_disableSound >
                <Icon name='arrow-back' size={wp * 0.075} color={'black'} />
            </Pressable>
            <TextInput
                placeholder='Search Treatment'
                style={styles.title}
            />
        </View>
    )
}

export default HeaderSearch

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderColor: 'rgba(55, 55, 55, 0.2)',
        paddingHorizontal: 16,
        paddingVertical: 7,
        backgroundColor: colors.white
    },
    title: {
        paddingVertical: 5,
        fontWeight: 'bold',
        color: 'black',
        fontSize: wp * 0.04,
        marginLeft: 10,
        flex: 1
    }
})
