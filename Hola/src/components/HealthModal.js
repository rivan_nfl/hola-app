import React, { useState } from 'react'
import { Alert, Modal, Pressable, StyleSheet, Text, View } from 'react-native'

import Icon from 'react-native-vector-icons/Ionicons'

import { widthPercentage as wp } from '../utils/dimension'
import { colors } from '../utils/colors'

const HealthModal = ({ modalVisible, closeModal, handleHealthModal }) => {
    const [vaccineChecked, setVaccineChecked] = useState(false);
    const [healthyChecked, setHealthyChecked] = useState(false);

    return (
        <View style={styles.container}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    setVaccineChecked(false)
                    setHealthyChecked(false)
                    closeModal();
                }}
            >   
                <View style={styles.modalContainer}>
                    <View style={styles.modalView}>
                        <Text style={[styles.modalTitle, {marginBottom: 16}]}>Health Declaration</Text>
                        <Text style={styles.modalText}>If you are feeling ill or show ing covid-19 symptoms, please reschedule / cancel your appointment. If you are showing signs of illness or not wearing a mask at time of service, our service providers have the right to leave with full payments.</Text>

                        <View style={{flexDirection: 'row', alignItems: 'center', marginBottom: 10}}>
                            <Pressable onPress={() => setVaccineChecked(!vaccineChecked)}>
                                <Icon name={vaccineChecked ? 'checkmark-circle' : 'ellipse-outline'} color={colors.gold} size={wp * 0.06} style={{marginRight: 12}} />
                            </Pressable>
                            <Text style={[styles.modalTitle, {flex: 1}]}>I'm Vaccinated</Text>
                        </View>
                        <View style={{flexDirection: 'row', alignItems: 'center', marginBottom: 20}}>
                            <Pressable onPress={() => setHealthyChecked(!healthyChecked)}>
                                <Icon name={healthyChecked ? 'checkmark-circle' : 'ellipse-outline'} color={colors.gold} size={wp * 0.06} style={{marginRight: 12}} />
                            </Pressable>
                            <Text style={[styles.modalTitle, {flex: 1}]}>I'm healthy, don't have any Covid-19 symptom</Text>
                        </View>
                        
                        <Pressable
                            style={styles.button}
                            onPress={() => {
                                vaccineChecked && healthyChecked ? (
                                    handleHealthModal(),
                                    setHealthyChecked(false),
                                    setVaccineChecked(false)
                                ) : alert('Please Check all the box given')   
                            }}
                        >
                            <Text style={styles.modalTitle}>Yes, I am Healthy</Text>
                        </Pressable>
                    </View>
                </View>
            </Modal>
        </View>
    )
}

export default HealthModal

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    modalContainer: {
        flex: 1,
        backgroundColor: 'rgba(55, 55, 55, 0.5)',
        justifyContent: 'center',
        paddingHorizontal: 12
    },
    modalView: {
        backgroundColor: 'white',
        borderRadius: 8,
        padding: 14
    },
    modalTitle: {
        fontSize: wp * 0.04,
        fontWeight: 'bold',
        color: 'black',
    },
    modalText: {
        marginBottom: 20,
        lineHeight: 20,
        color: 'black',
        fontSize: wp * 0.04
    },
    button: {
        backgroundColor: colors.gold,
        paddingVertical: 10,
        alignItems: 'center',
        borderRadius: 8
    }
})
