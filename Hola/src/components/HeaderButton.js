import React from 'react'
import { Pressable, StyleSheet, Text, View } from 'react-native'

import Icon from 'react-native-vector-icons/Ionicons'
import { colors } from '../utils/colors'
import { widthPercentage as wp } from '../utils/dimension'

const HeaderButton = ({ title, onPress }) => {
    return (
        <View style={styles.container}>
            <Pressable onPress={onPress} android_disableSound >
                <Icon name='arrow-back' size={wp * 0.075} color={'black'} />
            </Pressable>
            <Text style={styles.title}>{title}</Text>
        </View>
    )
}

export default HeaderButton

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderColor: 'rgba(55, 55, 55, 0.2)',
        paddingHorizontal: 16,
        paddingVertical: 10,
        backgroundColor: colors.white
    },
    title: {
        fontWeight: 'bold',
        color: 'black',
        fontSize: wp * 0.05,
        marginLeft: 12
    }
})
