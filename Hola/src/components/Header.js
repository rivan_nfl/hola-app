import React from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'

import Icon from 'react-native-vector-icons/Ionicons'
import holaIcon from '../assets/hola-icon.png'

import { colors } from '../utils/colors'
import { widthPercentage as wp,heightPercentage as hp } from '../utils/dimension'

const Header = () => {
    return (
        <View style={styles.container}>
            <Image source={holaIcon} style={styles.img} />
            <Icon name='notifications' size={wp * 0.08} color={colors.gold} />
        </View>
    )
}

export default Header

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        borderBottomWidth: 1,
        borderColor: 'rgba(55, 55, 55, 0.2)',
        paddingHorizontal: 16,
        paddingVertical: 10,
        backgroundColor: colors.white
    },
    img: {
        height: wp * 0.08
    }
})