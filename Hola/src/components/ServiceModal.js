import React, { useState } from 'react'
import { Alert, Modal, Pressable, StyleSheet, Text, View } from 'react-native'

import Icon from 'react-native-vector-icons/Ionicons'

import { widthPercentage as wp } from '../utils/dimension'
import { colors } from '../utils/colors'

const ServiceModal = ({ modalVisible, closeModal }) => {
    const [checked, setChecked] = useState('clinic');

    const dataService = {
        bba: {
            loc: 'BBA Gunawarman',
            address: 'Jl Kemang Raya 18D, Jakarta Selatan',
            icon: 'business'
        },
        lash: {
            loc: 'Lash Studio',
            address: 'Jl Kemang Raya 18D, Jakarta Selatan',
            icon: 'business'
        },
        home: {
            loc: 'Home Service',
            address: 'Just take your time, we come to your home',
            icon: 'home'
        }
    }

    const handleData = () => {
        checked == 'BBA Gunawarman'
        ? closeModal(dataService.bba)
        : checked == 'Lash Studio'
        ? closeModal(dataService.lash)
        : checked == 'Home Service'
        ? closeModal(dataService.home)
        : closeModal(null)
    }

    return (
        <View style={styles.container}>
            <Modal
                animationType="slide"
                transparent={true}
                visible={modalVisible}
                onRequestClose={() => {
                    handleData();
                }}
            >   
                <View style={styles.modalContainer}>
                    <View style={styles.modalView}>
                        <View style={{flexDirection: 'row', alignItems: 'center', marginBottom: 16}}>
                            <Text style={[styles.modalTitle, {flex: 1}]}>Select Services Type</Text>
                            <Pressable onPress={() => handleData()}>
                                <Icon name='close-outline' color={'black'} size={wp * 0.085} />
                            </Pressable>
                        </View>
                        
                        <View style={{marginBottom: 20}}>
                            <Text style={styles.medTxt}>On Clinic</Text>
                            <Text>Choose Clinic Location</Text>
                        </View>

                        <Pressable
                            style={[styles.servicesLoc, checked == 'BBA Gunawarman' && styles.servicesLocSelected]}
                            onPress={() => setChecked('BBA Gunawarman')}
                            android_disableSound
                        >
                            <View style={[styles.locIcon, checked == 'BBA Gunawarman' && styles.locIconSelected]}>
                                <Icon name='business' size={wp * 0.07} color={colors.gold} />
                            </View>
                            <View style={{flex: 1}}>
                                <Text style={styles.medTxt}>BBA Gunawarman</Text>
                                <Text>Jl Kemang Raya 18D, Jakarta Selatan</Text>
                            </View>
                        </Pressable>

                        <Pressable
                            style={[styles.servicesLoc, checked == 'Lash Studio' && styles.servicesLocSelected]}
                            onPress={() => setChecked('Lash Studio')}
                            android_disableSound
                        >
                            <View style={[styles.locIcon, checked == 'Lash Studio' && styles.locIconSelected]}>
                                <Icon name='business' size={wp * 0.07} color={colors.gold} />
                            </View>
                            <View style={{flex: 1}}>
                                <Text style={styles.medTxt}>Lash Studio</Text>
                                <Text>Jl Kemang Raya 18D, Jakarta Selatan</Text>
                            </View>
                        </Pressable>

                        <Pressable
                            style={[styles.servicesLoc, checked == 'Home Service' && styles.servicesLocSelected]}
                            onPress={() => setChecked('Home Service')}
                            android_disableSound
                        >
                            <View style={[styles.locIcon, checked == 'Home Service' && styles.locIconSelected]}>
                                <Icon name='home' size={wp * 0.07} color={colors.gold} />
                            </View>
                            <View style={{flex: 1}}>
                                <Text style={styles.medTxt}>Home Service</Text>
                                <Text>Just take your time, we come to your home</Text>
                            </View>
                        </Pressable>

                    </View>
                </View>
            </Modal>
        </View>
    )
}

export default ServiceModal

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    modalContainer: {
        flex: 1,
        justifyContent: 'flex-end',
        backgroundColor: 'rgba(55, 55, 55, 0.5)',
    },
    modalView: {
        backgroundColor: 'white',
        borderRadius: 8,
        padding: 14,
        paddingBottom: 0
    },
    modalTitle: {
        fontSize: wp * 0.05,
        fontWeight: 'bold',
        color: 'black',
    },
    modalText: {
        marginBottom: 20,
        lineHeight: 20,
        color: 'black',
        fontSize: wp * 0.04
    },
    button: {
        backgroundColor: colors.gold,
        paddingVertical: 10,
        alignItems: 'center',
        borderRadius: 8
    },
    medTxt: {
        fontSize: wp * 0.04,
        fontWeight: 'bold',
        color: 'black',
    },
    servicesLoc: {
        flexDirection: 'row',
        alignItems: 'center',
        marginBottom: 20,
        padding: 10,
        borderRadius: 8
    },
    servicesLocSelected: {
        backgroundColor: 'rgba(55, 55, 55, 0.3)'
    },
    locIcon: {
        padding: 5,
        backgroundColor: 'rgba(55, 55, 55, 0.07)',
        borderRadius: 8,
        marginRight: 12
    },
    locIconSelected: {
        backgroundColor: 'white'
    }
})
