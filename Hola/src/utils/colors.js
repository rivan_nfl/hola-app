export const colors = {
    gold: '#FFAB76',
    white: '#FCFCFC',
    pink: '#EED7CE',
    darkBlue: '#5584AC'
}