import { Dimensions } from "react-native"

export const widthPercentage = Dimensions.get('screen').width
export const heightPercentage = Dimensions.get('screen').height